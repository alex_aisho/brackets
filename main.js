let str = '(([(]))';


let a = [0];
let b = [0];
let fail = false;

for (s of str) {
	if (s == '(') {
		a[a.length - 1]++;
		b.push(0);
	}
	if (s == '[') {
		b[b.length - 1]++;
		a.push(0);
	}
	if (s == ')') {
		a[a.length - 1]--;
		if (b.pop() !== 0 || b.length == 0) {
            fail = true;
            break;
		}
	}
	if (s == ']') {
		b[b.length - 1]--;
		if (a.pop() !== 0 || a.length == 0) {
            fail = true;
            break;
		}
	}
}
if (fail || (b[0]!==0 || a[0] !== 0))
{
	console.log('fail');
} else 
   console.log('OK!');